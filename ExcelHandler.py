
import pandas as pd
from openpyxl.styles import PatternFill
from urllib.parse import urlparse, parse_qs
from openpyxl import load_workbook


def append_dataframe_to_excel(diccionario, filename, sheet_index):      
    '''
    takes a diccionary and convert it to a dataframe wich is pasted on an excel file
    marks in red the pokemons who weights more than 100 kg
    '''
    i = 0
    df = pd.DataFrame(diccionario)
    print(df)
    # Crear un objeto PatternFill con el color deseado
    relleno = PatternFill(start_color="FF0000", end_color="FF0000", fill_type="solid")  # Color rojo

    # Crear un objeto ExcelWriter utilizando openpyxl
    nombre_archivo = filename
    hoja_nombre = sheet_index
    df.to_excel(nombre_archivo, sheet_name=hoja_nombre, index=False)


    workbook = load_workbook(nombre_archivo)

    worksheet = workbook[hoja_nombre]

    rango = 'D'+ str(len(df)+1)
    rango_celdas = worksheet['A2':rango]  
    
    for fila in rango_celdas:
        for celda in fila:
            if diccionario['Peso'][i]>100:
                celda.fill = relleno
        i = i+1
    
    # Guardar el archivo Excel modificado
    workbook.save(nombre_archivo)
    workbook.close()