import yaml
import requests
import pandas as pd
import logging
from openpyxl.styles import PatternFill
from urllib.parse import urlparse, parse_qs
from openpyxl import load_workbook
import ExcelHandler as E

def get_all_pokemons(url1):
    '''
    gets the response from url1 and converts it to an array, wich is returned.
    '''
    pokemons_array = []
    numero_max = 0
    try:
        response_http = requests.get(url1)
        info = response_http.json()

        pokemons = info['results']           
        for k in pokemons:
            pokemons_array.append(info['results'][numero_max]['name'])
            numero_max = numero_max + 1

    except Exception as e:          
            logging.error(f'Error when doing request of the initial page. Reason: {e}')
            pokemons_array = []


    return pokemons_array


def f_tipos(n_tipos, tipo_peso, tipo):    
    '''
    gets the type of the pokemon and separates with "/" if there is more than one type. Returns a type array.
    '''
    f = 0
    types = ""
    for f in range(n_tipos):
        if f == 0:
            types = str(types) + tipo_peso['types'][f]['type']['name']
        else:
            types = str(types) + "/" + tipo_peso['types'][f]['type']['name']

    tipo.append(types)
    return tipo



def response_pokemons(pokemons_array, url2, n_pokemons):
    '''
    gets the array of pokemons names and make a request of each one.
    gets all the parameters (id, weight and type(s)) and join all of them in a single diccionary, wich is returned
    '''
    peso = []
    id = []
    tipo = []
    peticiones = []
    i = 0

    dicc_names = dict(zip(range(len(pokemons_array)), pokemons_array))

    for i in range(n_pokemons):
        peticiones.append(url2+str(dicc_names[i])+"/")

    for i in range(n_pokemons):
        try: 
            res_http_ind = requests.get(peticiones[i])
            tipo_peso = res_http_ind.json()
            peso.append(tipo_peso['weight'])
            id.append(tipo_peso['id'])
            n_tipos = len(tipo_peso['types'])
            tipo = f_tipos(n_tipos, tipo_peso, tipo)

        except Exception as e:          
            logging.error(f'Error when doing request of the pokemons. Reason: {e}')

    dicc_weights = dict(zip(range(len(peso)), peso))
    dicc_type = dict(zip(range(len(tipo)), tipo))
    dicc_ids = dict(zip(range(len(peso)), id))

    total = {
        'Id': dicc_ids,
        'Nombre': dicc_names,
        'Tipo': dicc_type,
        'Peso': dicc_weights
    }

    return total



def obtain_pokemons(url1):
    '''
    Obtains the number of pokemons of the url1
    '''
    parsed_url = urlparse(url1)
    params = parse_qs(parsed_url.query)
    limit = int(params['limit'][0])
    return limit
    


def main():

    with open("config.yaml", "r") as file:                      
        response = yaml.safe_load(file)                                    

    url1 = response['URL1']
    url2 = response['URL2']
    n_pokemons = obtain_pokemons(url1)
    pokemons_array = get_all_pokemons(url1)
    total = response_pokemons(pokemons_array, url2, n_pokemons)
    E.append_dataframe_to_excel(total, "reports\pokemons_info.xlsx", "hoja_pokemons")  


if __name__ == "__main__":
    main()