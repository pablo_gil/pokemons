BLIBIOTECAS USADAS:

    import yaml
        Biblioteca para trabajar con archivos yaml
    import requests
        Biblioteca para trabajar con solicitudes HTTP
    import pandas as pd
        Biblioteca usada para trabajar con analisis y manipulacion de datos en python
    import logging
        Biblioteca utilizada para mostrar mensajes
    from openpyxl.styles import PatternFill
        Biblioteca utilizada para manipular archivos excel en formato xlsx
    from urllib.parse import urlparse, parse_qs
        Biblioteca usada para manipuylar URLs